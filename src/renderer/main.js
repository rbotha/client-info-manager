import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import router from './router'
import { store } from './store'
import localforage from 'localforage'
import VueClip from 'vue-clip'
import Vuelidate from 'vuelidate'
import DateFilter from './filters/date'
import AlertCmp from './components/Shared/Alert.vue'
import MessageCmp from './components/Shared/MessageBox.vue'
import MessageConfirmCmp from './components/Shared/ConfirmBox.vue'
import EditAssetManagerDetailsDialog from './components/AssetManager/Edit/EditAssetManagerDetailsDialog.vue'
import EditAssetManagerDateDialog from './components/AssetManager/Edit/EditAssetManagerDateDialog.vue'
import EditAssetManagerTimeDialog from './components/AssetManager/Edit/EditAssetManagerTimeDialog.vue'
import RegisterDialog from './components/AssetManager/Registration/RegisterDialog.vue'
require('localforage-startswith')

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.config.productionTip = false
Vue.use(Vuetify)
Vue.use(VueClip)
Vue.use(Vuelidate)
Vue.filter('date', DateFilter)
Vue.component('app-alert', AlertCmp)
Vue.component('app-msg', MessageCmp)
Vue.component('app-msg-confirm', MessageConfirmCmp)
Vue.component('app-edit-assetmanager-details-dialog', EditAssetManagerDetailsDialog)
Vue.component('app-edit-assetmanager-date-dialog', EditAssetManagerDateDialog)
Vue.component('app-edit-assetmanager-time-dialog', EditAssetManagerTimeDialog)
Vue.component('app-register-assetmanager-dialog', RegisterDialog)

localforage.config({
  name: 'documents'
})
localforage.config({
  name: 'images'
})

export const eventBus = new Vue()

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
  }
})

/*
{
  "rules": {
    "managers" : {
       "$manager_email" : {
         ".read": "auth.uid != null",
          ".write": "auth.uid != null",
             "$uid": {
              ".read": "auth.uid === $uid",
              ".write": "auth.uid === $uid",
              },
             "dbkey": {
              ".read": "data.parent().child($manager_email).hasChild(auth.uid)",
             ".write": "data.parent().child($manager_email).hasChild(auth.uid)",
              }
      }
    },
    "clients" : {
       "$client_email" : {
         ".read": "auth.uid != null",
          ".write": "auth.uid != null",
             "$uid": {
              ".read": "auth.uid === $uid",
              ".write": "auth.uid === $uid",
              }
          }
     }
  }
}
 */
