/*
TODO: reset password
TODO: enable send emails - host, port
TODO: change email address
*/
const actions = {
  authStateChange ({commit, rootGetters}) {
    console.log('AUTH STATE CHANGE')
    rootGetters.auth.onAuthStateChanged((user) => {
      if (user) {
        console.log(`auth state change -- received user ${user.uid}`)
        const newUser = {
          uid: user.uid,
          email: user.email,
          verified: user.emailVerified,
          isEmailSent: rootGetters['settings/isEmailSent']
        }
        console.log(`AUTH STATE: ${user.uid}`)
        commit('updateUser', newUser, {root: true})
        if (!newUser.isEmailSent) {
          rootGetters.auth.currentUser.reload()
          user.sendEmailVerification()
            .then(() => {
              console.log('email sent ' + user.email)
              commit('setInfo',
                {
                  title: 'Verify Email',
                  message: `Email sent to ${user.email}, please verify email link and sign in`
                }, {root: true})
              commit('setEmailSent', true, {root: true})
            })
        }
        if (user.emailVerified) {
          console.log('call root load - authstatechange')
          commit('load', {}, {root: true})
        }
      }
    })
  },
  signUserUp ({commit, rootGetters}, payload) {
    console.log('signuserUp')
    commit('setLoading', true, {root: true})
    commit('setError', null, {root: true})
    commit('setInfo', null, {root: true})
    rootGetters.auth.createUserWithEmailAndPassword(payload.email, payload.password)
      .then(
        user => {
          console.log(`User: ${user.toJSON()}`)
          commit('setLoading', false, {root: true})
          const newUser = {
            uid: user.uid,
            email: payload.email,
            verified: user.emailVerified,
            emailSent: false // ,fbkeys
          }
          commit('setUser', newUser, {root: true})
        }
      )
      .catch(
        error => {
          console.log(`Error: ${error.code} : ${error.message}`)
          // auth/invalid-email, auth/weak-password, auth/email-already-in-use
          commit('setLoading', false, {root: true})
          commit('setError', error.message, {root: true})
        }
      )
  },
  signUserIn ({commit, rootGetters}, payload) {
    console.log('................sign in.....................')
    commit('setLoading', true, {root: true})
    commit('setError', null, {root: true})
    commit('setInfo', null, {root: true})
    rootGetters.auth.signInWithEmailAndPassword(payload.email, payload.password)
      .then(
        user => {
          console.log(`sign in ${user.uid}`)
          const newUser = {
            uid: user.uid,
            email: payload.email,
            verified: user.emailVerified,
            emailSent: rootGetters['settings/isEmailSent'] // ,fbkeys
          }
          console.log(`SIGN IN: ${user.uid}`)
          commit('setUser', newUser, {root: true})
        }
      )
      .catch(
        error => {
          // auth/invalid-email, auth/user-disabled, auth/user-not-found, auth/wrong-password
          commit('setLoading', false, {root: true})
          commit('setError', error.message, {root: true})
          console.log(`${error.message}`)
        }
      )
  },
  signOut ({commit, rootGetters}) {
    console.log('sign out')
    rootGetters.auth.signOut().then(() => {
      commit('setInfo',
        {
          title: 'Signed out',
          message: `User successfully signed out`
        })
      commit('setUser', null, {root: true})
    }, (error) => {
      commit('setError', error.message, {root: true})
    })
  }
}

export const User = {
  namespaced: true,
  actions
}
