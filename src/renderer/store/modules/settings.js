const settings = require('electron').remote.require('electron-settings')
const app = require('electron').remote.app

const sections = [
  {key: 'data', title: 'Data', description: 'Forms and database', icon: 'cloud_circle', color: ['orange--text', 'text--darken-2']},
  {key: 'user', title: 'User', description: 'User settings', icon: 'account_circle', color: ['green--text', 'text--darken-2']},
  {key: 'app', title: 'Application', description: 'Application Information', icon: 'settings_applications', color: ['primary--text', 'text--darken-2']}
]

const initialize = (state) => {
  console.log('===========================mutations - initSettings')
  // state.settings = settingsStore.getAll()
  // Add rest of default settings if not already in store
  let documents = app.getPath('documents')
  if (!state.settings.has('sections')) {
    state.settings.set('sections', sections)
    // Forms Directory
    if (!state.settings.has('forms_folder')) {
      let forms = {
        title: 'Directory location of input forms (.pdf)',
        directory: documents,
        editable: true,
        type: 'file',
        subheader: 'Forms Folder',
        label: 'Folder Name'
      }
      state.settings.set('data.forms_folder', forms)
    }
    // Database Directory
    if (!state.settings.has('database_folder')) {
      let backups = {
        title: 'Directory location of application database',
        directory: documents,
        editable: true,
        type: 'file',
        subheader: 'Database Folder',
        label: 'Folder Name'
      }
      state.settings.set('data.database_folder', backups)
    }
    // Backup Directory
    if (!state.settings.has('backup_folder')) {
      let backups = {
        title: 'Directory location of backups',
        directory: documents,
        editable: true,
        type: 'file',
        subheader: 'Backup Folder',
        label: 'Folder Name'
      }
      state.settings.set('data.backup_folder', backups)
    }
    // User Email
    if (!state.settings.has('user')) {
      let user = {
        title: 'Application email',
        type: 'text',
        editable: false,
        value: '', // TODO: get from login
        verified: false,
        emailSent: false,
        subheader: 'Email',
        label: 'Change in login process'
      }
      console.log('add user to settings')
      state.settings.set('user.email', user)
    } else {
      console.log('state has user')
    }
  }
}

const state = {
  settings
}

const mutations = {
  initSettings (state) {
    initialize(state)
  },
  setField (state, {key, value}) {
    console.log(`set key=${key} value=${value}`)
    state.settings.set(key, value)
  },
  reset (state) { // TODO: test!!
    state.settings.deleteAll()
    initialize(state)
  },
  setEmail (state, value) {
    state.settings.set('user.email.value', value)
  },
  setVerified (state, value) {
    state.settings.set('user.email.verified', value)
  },
  setEmailSent (state, value) {
    state.settings.set('user.email.sent', value)
  }
}

const actions = {
  initSettings ({state, commit, rootState, rootGetters}) {
    console.log('===========================actions - initSettings')
    commit('initSettings')
  },
  setField ({state, commit}, payload) {
    console.log(`payload ${payload}`)
    commit('setField', payload)
  },
  setEmail ({commit}, value) {
    commit('setEmail', value)
  },
  setVerified ({commit}, value) {
    commit('setVerified', value)
  },
  setEmailSent ({commit}, value) {
    commit('setEmailSent', value)
  }
}

const getters = {
  getSections (state) {
    return state.settings.get('sections')
  },
  getKeys: (state) => (section) => {
    console.log(state.settings.get(section))
    if (state.settings.has(section)) {
      let keys = Object.keys(state.settings.get(section))
      console.log(`section ${section} : ${keys}`)
      return Object.keys(state.settings.get(section))
    } else {
      return ''
    }
  },
  hasField: (state) => (setting) => {
    console.log(setting)
    return state.settings.has(setting) // `${setting.key}.${setting.field}`)
  },
  getField: (state) => (setting) => {
    console.log(`getField ${setting}`)
    return state.settings.get(setting)
  },
  getSetting: (state) => (key) => {
    console.log(`setting key: ${key}`)
    return state.settings.get(key)
  },
  getEmail (state) {
    return state.settings.get('user.email.value')
  },
  isVerified (state) {
    return state.settings.get('user.email.verified')
  },
  isEmailSent (state) {
    return state.settings.get('user.email.sent')
  }
}

export const Settings = {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
}
