import Dexie from 'dexie'

const db = new Dexie('cimDB')

// You only need to specify properties that you wish to index
// http://dexie.org/docs/Version/Version.stores().html
db.version(1).stores({
  documents: '&hashidForm,title,fileName',
  clients: '&IdPassportNumber,Name,Surname,EmailAddress',
  images: '&hashid,file'
})

db.open()
  .catch((e) => {
    console.log(e.stack)
  })
export default db
