export default {
  state: {
    error: '',
    loading: false,
    info: ''
  },
  mutations: {
    setInfo (state, payload) {
      state.info = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    setLoading (state, payload) {
      state.loading = payload
      // console.log(`set loading: ${state.loading}`)
    }
  },
  actions: {
    setInfo ({commit}, payload) {
      commit('setInfo', payload)
    },
    setError ({commit}, payload) {
      commit('setError', payload)
    },
    setLoading ({commit}, payload) {
      commit('setLoading', payload)
    }
  },
  getters: {
    loading (state) {
      // console.log(`get loading: ${state.loading}`)
      return state.loading
    },
    info (state) {
      return state.info
    },
    error (state) {
      return state.error
    }
  }
}
