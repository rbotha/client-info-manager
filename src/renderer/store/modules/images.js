import Vue from 'vue'
import crypto from 'crypto'
import db from './dexieStorage'
import { nativeImage } from 'electron'

const state = {
  images: {},
  image: {
    data: null,
    thumb: null,
    filename: '',
    hashid: '',
    created: '',
    usedBy: []
  },
  title: ''
}

const mutations = {
  load (state, payload) {
    state.images = payload
  },
  add (state, payload) {
    Vue.set(state.images, payload.hashid, payload)
  },
  remove (state, payload) {
    Vue.delete(state.images, payload)
  },
  update (state, payload) {
    state.images[payload.hashid].title = payload.title
  },
  reset (state) {
    state.image = Object.assign({}, state.image, {
      data: null,
      thumb: null,
      filename: ''
    })
    state.title = ''
  },
  setImage (state, payload) {
    state.image = Object.assign({}, state.image, payload)
  },
  setTitle (state, payload) {
    state.title = payload
  },
  setCreated (state, payload) {
    state.image.created = payload
  },
  setUsedBy (state, payload) {
    state.image.usedBy.unshift(payload)
  }
}

const actions = {
  load ({state, commit}) {
    if (state.images || Object.keys(state.images).length === 0) {
      let images = {}
      db.images.toCollection().each((image) => {
        images[image.hashid] = image
      })
      .then(() => {
        commit('load', images)
      })
      .catch((error) => {
        commit('setError', `Load img error: ${error.message}`, {root: true})
      })
    }
  },
  add ({commit}, payload) {
    commit('setInfo', '', {root: true})
    commit('setError', null, {root: true})
    commit('setLoading', true, {root: true})
    const hash = crypto.createHash('md5')
    let hashid = hash.update(payload.data).digest('hex')
    return new Promise((resolve, reject) => {
      if (hashid in state.images) {
        let msg = `Image already exist - Title: ${state.images[hashid].title}`
        commit('setError', msg, {root: true})
        commit('setLoading', false, {root: true})
        resolve(msg)
      } else {
        let image = {
          title: payload.title,
          filename: payload.filename,
          data: payload.data,
          hashid: hashid,
          thumb: payload.thumb,
          created: new Date()
        }
        db.images.add(image)
          .then((id) => {
            commit('add', image)
            commit('setLoading', false, {root: true})
            resolve(`Image succesfully added`)
          })
          .catch((error) => {
            commit('setError', error.message, {root: true})
            console.log(`add img error: ${error.message}`, {root: true})
          })
      }
    })
  },
  remove ({state, commit}, payload) { // todo: only if not used
    commit('setError', null, {root: true})
    return new Promise((resolve, reject) => {
      db.images.where('hashid').equals(payload).delete()
        .then(() => {
          console.log(`found ${payload} - removed`)
          commit('remove', payload)
          resolve(payload)
        })
        .catch((error) => {
          commit('setError', `del img error: ${error.message}`, {root: true})
          // reject(error)
        })
    })
  },
  update ({commit}, payload) {
    commit('setError', null, {root: true})
    return new Promise((resolve, reject) => {
      db.images.update(payload.hashid, {title: payload.title})
        .then((updated) => {
          commit('update', payload)
          resolve(updated)
        })
        .catch((error) => {
          commit('setError', `del img error: ${error.message}`, {root: true})
          // reject(error)
        })
    })
  },
  upload ({commit, getters}, payload) {
    commit('setInfo', '', {root: true})
    commit('setError', '', {root: true})
    commit('setLoading', true, {root: true})
    return new Promise((resolve, reject) => {
      let image = nativeImage.createFromPath(payload)
      if (image) {
        const hash = crypto.createHash('md5')
        let data = image.toDataURL()
        let hashid = hash.update(data).digest('hex')
        let thumb = image.resize({height: 150, quality: 'best'})
        let newImage = {
          hashid: hashid,
          filename: payload,
          data: data,
          thumb: thumb.toDataURL(),
          title: ''
        }
        commit('setImage', newImage)
        commit('setLoading', false, {root: true})
        resolve(newImage)
      } else {
        commit('setLoading', false, {root: true})
        commit('setError', 'invalid image', {root: true})
      }
    })
  },
  reset ({commit}) {
    commit('reset')
  },
  setTitle ({commit}, payload) {
    commit('setTitle', payload)
  },
  setImage ({commit}, payload) {
    commit('setImage', payload)
  },
  setCreated ({commit}) {
    commit('setCreated', new Date())
  },
  setUsedBy ({commit}, payload) {
    commit('setUsedBy', payload)
  }
}

const getters = {
  getImageThumb: state => id => {
    /* db.images.get({hashid: id}, image => {
      return image.thumb
    })
    .catch((error) => {
      console.log(`getImage error: ${error.message}`)
    })
    */
    return state.images[id].thumb
  },
  hasImage: state => data => {
    const hash = crypto.createHash('md5')
    let hashid = hash.update(data).digest('hex')
    if (hashid in state.images) {
      return state.images[hashid].title
    } else {
      return ''
    }
  },
  getImages: state => search => {
    let filtered = []
    let unfiltered = state.images
    Object.keys(unfiltered).filter(id => {
      if (unfiltered[id].title.toLowerCase().includes(search.toLowerCase())) {
        filtered.push(unfiltered[id])
      }
    })
    return filtered
  },
  getUploadedImage (state) {
    return state.image
  },
  getTitle (state) {
    return state.title
  },
  getImage: state => id => {
    if (state.image && state.image.hashid === id) {
      return state.image
    } else {
      return state.images[id]
    }
  },
  size (state) {
    return Object.keys(state.images).length
  }
}

export const ImagesDB = {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
}
