import Vue from 'vue'
import crypto from 'crypto'
import db from './dexieStorage'
const fs = require('fs')
// import { saveItem, removeItem, fetchItems, deleteItems } from './forage'
const path = require('path')
const hummus = require('hummus')
const PDFDigitalForm = require('./pdf-digital-form')

// const DOC_NAMESPACE = 'DOC-'

const state = {
  documents: {},
  loaded: false,
  hasBase: false,
  document: {
    filename: '',
    fields: [],
    hashid: ''
  }
}

const mutations = {
  load (state, payload) {
    state.documents = payload
    state.loaded = true
  },
  add (state, payload) {
    console.log(`add ${payload.filename}`)
    Vue.set(state.documents, payload.hashid, payload)
  },
  remove (state, payload) {
    Vue.delete(state.documents, payload)
  },
  update (state, payload) {
    state.documents[payload.hashid].title = payload.title
  },
  deleteAll (state) {
    state.document = Object.assign({}, state.document, {})
  },
  setBase (state, payload) {
    state.hasBase = payload
  },
  setDocument (state, payload) {
    state.document = Object.assign({}, state.document, payload)
  }
}

const actions = {
  load ({state, commit}) {
    commit('setError', null, {root: true})
    commit('setLoading', true, {root: true})
    console.log(`documents.js - load action ${Object.keys(state.documents).length}`)
    // load documents only if they are not loaded already
    if (state.documents || Object.keys(state.documents).length === 0) {
      let docs = {}
      db.documents.toCollection().each((doc) => {
        docs[doc.id] = doc
        if (doc.isBase) {
          commit('setBase', true)
        }
      })
      .then(() => {
        commit('load', docs)
        commit('setLoading', false, {root: true})
      })
      .catch((error) => {
        commit('setError', error.message, {root: true})
        commit('setLoading', false, {root: true})
      })
    }
  },
  add ({state, commit}, payload) {
    commit('setError', null, {root: true})
    if (payload.isBase) {
      commit('setBase', true)
    }
    return new Promise((resolve, reject) => {
      if (payload.hashidForm in state.documents) {
        let msg = `Document already exist - Title: ${state.documents[payload.hashidForm].fileName}`
        commit('setError', msg, {root: true})
        resolve(msg)
      } else {
        let document = {
          title: payload.title,
          fileName: payload.fileName,
          formName: payload.formName,
          hashidImage: payload.hashidImage,
          hashidForm: payload.hashidForm,
          description: payload.description,
          isBase: payload.isBase,
          fields: payload.fields
        }
        db.documents.add(payload)
          .then(id => {
            resolve(`Image succesfully added`)
            commit('add', document)
          })
          .catch((error) => {
            commit('setError', error.message, {root: true})
            console.log(`add  doc error: ${error.message}`)
          })
      }
    })
  },
  remove ({state, commit}, payload) {
    commit('setError', null, {root: true})
    if (payload.isBase) {
      commit('setBase', true)
    }
    return new Promise((resolve, reject) => {
      db.documents.where('hashidForm').equals(payload.hashidForm).delete()
        .then(() => {
          console.log(`found ${payload} - removed`)
          commit('remove', payload)
          resolve(payload)
        })
        .catch((error) => {
          commit('setError', `delete document error: ${error.message}`, {root: true})
        })
    })
  },
  update ({state, commit}, payload) {
    commit('setError', null, {root: true})
    if (payload.isBase) {
      commit('setBase', true)
    }
    return new Promise((resolve, reject) => {
      db.documents.update(payload.hashid, payload)  // todo: what can be updated
        .then((updated) => {
          commit('update', payload)
          resolve(updated)
        })
        .catch((error) => {
          commit('setError', `update doc error: ${error.message}`, {root: true})
        })
    })
  },
  parsePDF ({commit}, payload) {
    commit('setLoading', true, {root: true})
    commit('setError', null, {root: true})
    return new Promise((resolve, reject) => {
      if (path.extname(payload).toLowerCase() === '.pdf') {
        // let noExt = path.basename(file, path.extname(file))
        let pdfParser = hummus.createReader(payload)
        let digitalForm = new PDFDigitalForm(pdfParser)
        if (digitalForm.hasForm()) {
          let fields = []
          digitalForm.fields.forEach((field) => {
            // console.log(`Name: ${field.name} type: ${field.type}`)
            fields.push({name: field.name, fullName: field.fullName, type: field.type})
          })
          let data = fs.readFileSync(payload)
          const hash = crypto.createHash('md5')
          let hashid = hash.update(data).digest('hex')
          console.log(`hashid: ${hashid}`)
          let document = {filename: payload, fields: fields, hashid: hashid}
          commit('setDocument', document)
          commit('setLoading', false, {root: true})
          resolve(document)
        }
      } else {
        commit('setError', 'invalid document', {root: true})
        commit('setLoading', false, {root: true})
      }
    })
  }
}

const getters = {
  getDocuments: state => search => {
    let filtered = []
    let unfiltered = state.documents
    Object.keys(unfiltered).filter(id => {
      if (unfiltered[id].title.toLowerCase().includes(search.toLowerCase())) {
        filtered.push(unfiltered[id])
      }
    })
    return filtered
  },
  getDocument: (state) => (id) => {
    console.log(`getDocument[${JSON.stringify(id)}]`)
    return state.documents[id]
  },
  count (state) {
    console.log(`count ${Object.keys(state.documents).length}`)
    return Object.keys(state.documents).length
  },
  hasBaseDocument () {
    return state.hasBase
  },
  loaded (state) {
    console.log(`loaded: ${state.loaded}`)
    return state.loaded
  }
}

export const Documents = {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
