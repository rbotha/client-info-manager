const state = {
  loadedAssetManagers: [],
  registeredAssetManagers: [],
  fbkeys: []
}

const mutations = {
  createAssetManager (state, payload) {
    // https://youtu.be/xwx4ua056Nc
    state.loadedAssetManagers.push(payload)
  },
  setLoadedAssetManagers (state, payload) {
    state.loadedAssetManagers = payload
  },
  updateAssetManager (state, payload) {
    const assetManager = state.loadedAssetManagers.find(assetmanager => {
      return assetmanager.id === payload.id
    })
    if (payload.title) {
      assetManager.title = payload.title
    }
    if (payload.description) {
      assetManager.description = payload.description
    }
    if (payload.date) {
      assetManager.date = payload.date
    }
  },
  registerUserForAssetManager (state, payload) {
    const id = payload.id
    if (state.registeredAssetManagers.findIndex(assetmanager => assetmanager.id === id) < 0) {
      state.registeredAssetManagers.push(id)
      state.fbkeys[id] = payload.fbkey
    }
  },
  unregisterUserForAssetManager (state, payload) {
    const id = payload.id
    state.registeredAssetManagers.splice(state.registeredAssetManagers.findIndex(assetmanager => assetmanager.id === id), 1)
    Reflect.deleteProperty(state.fbkeys, payload)
  }
}

const actions = {
  registerUserForAssetManager ({commit, rootGetters}, payload) {
    commit('setLoading', true, {root: true})
    const user = rootGetters.getUser
    rootGetters.database.ref('/users/' + user.id).child('/registrations/')
      .push(payload)
      .then(data => {
        commit('setLoading', false, {root: true})
        commit('registerUserForAssetManager', {id: payload, fbkey: data.key})
      })
      .catch((err) => {
        console.log(`err ${err}`)
        commit('setLoading', false, {root: true})
      })
  },
  unregisterUserForAssetManager ({commit, rootGetters, getters}, payload) {
    commit('setLoading', true, {root: true})
    const user = rootGetters.getUser
    const fbkeys = getters.assetmanagerKeys
    if (!fbkeys) {
      return
    }
    const fbKey = fbkeys[payload]
    rootGetters.database.ref('/users/' + user.id + '/registrations/').child(fbKey)
      .remove()
      .then(() => {
        commit('setLoading', false, {root: true})
        commit('unregisterUserForAssetManager', payload)
      })
      .catch((err) => {
        console.log(`err ${err}`)
        commit('setLoading', false, {root: true})
      })
  },
  loadAssetManagers ({commit, rootGetters}) {
    commit('setLoading', true, {root: true})
    rootGetters.database.ref('AssetManagers').once('value')
      .then((data) => {
        const assetManagers = []
        const obj = data.val()
        for (let key in obj) {
          assetManagers.push({
            id: key,
            title: obj[key].title,
            date: obj[key].date,
            location: obj[key].location,
            description: obj[key].description,
            imageId: obj[key].imageId,
            creatorId: obj[key].creatorId
          })
        }
        commit('setLoadedAssetManagers', assetManagers)
        commit('setLoading', false, {root: true})
      })
      .catch((err) => {
        console.log(`err ${err}`)
        commit('setLoading', false, {root: true})
      })
  },
  createAssetManager ({context, commit, rootGetters}, payload) {
    const assetManager = {
      title: payload.title,
      location: payload.location,
      description: payload.description,
      date: payload.date.toISOString(),
      creatorId: rootGetters.getUser.uid,
      imageId: payload.imageId
    }
    rootGetters.database.ref('AssetManagers').push(assetManager)
      .then(() => {
        commit('createAssetManager', assetManager)
      })
      .catch((err) => {
        console.log(`err ${err}`)
      })
  },
  updateAssetManagerData ({commit, rootGetters}, payload) {
    commit('setLoading', true, {root: true})
    console.log(`update ${payload.id} ${payload.date}`)
    const updateObj = {}
    if (payload.title) {
      updateObj.title = payload.title
    }
    if (payload.description) {
      updateObj.description = payload.description
    }
    if (payload.date) {
      updateObj.date = payload.date
    }
    rootGetters.database.ref('AssetManagers').child(payload.id).update(updateObj)
      .then(() => {
        commit('setLoading', false, {root: true})
        commit('updateAssetManager', payload)
      })
      .catch(error => {
        console.log(`update error: ${error}`)
        commit('setLoading', false, {root: true})
      })
  }
}

const getters = {
  assetmanagerKeys (state) {
    return state.fbkeys
  },
  loadedAssetManagers (state) {
    return state.loadedAssetManagers.sort((assetManagerA, assetManagerB) => {
      return assetManagerA.date > assetManagerB.date
    })
  },
  featuredAssetManagers (state, getters) {
    return getters.loadedAssetManagers.slice(0, 5)
  },
  loadedAssetManager (state) {
    return (assetmanagerId) => {
      return state.loadedAssetManagers.find((assetmanager) => {
        return assetmanager.id === assetmanagerId
      })
    }
  },
  registerUserForAssetManager (state) {
    return state.registeredAssetManagers
  }
}

export const AssetManagers = {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
