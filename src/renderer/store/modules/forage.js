import localforage from 'localforage'
// Dexie.org ???

export const fetchItems = (namespace) => {
  return localforage.startsWith(namespace)
    .then((res) => {
      return res
    })
}
export const saveItem = ({item, namespace}) => {
  console.log(namespace + item.id)
  return localforage.setItem(
    namespace + item.id,
    item)
    .then((value) => {
      return value
    }).catch((err) => {
      console.log(`Error saving item: ${err.message}`)
    })
}

export const removeItem = ({item, namespace}) => {
  return localforage.removeItem(
    namespace + item.id,
    item)
    .then(() => {
      return true
    }).catch((err) => {
      console.log(`Error removing doc: ${err.message}`)
      return false
    })
}

export const deleteItems = (namespace) => {
  return localforage.startsWith(namespace)
    .then((res) => {
      Object.keys(res).forEach((key) => {
        localforage.removeItem(
          namespace + res[key].id, res[key])
      })
    })
}
