import Vue from 'vue'
import Vuex from 'vuex'
import crypto from 'crypto'
import * as firebase from 'firebase'
// import fs from 'fs'
import uuidv4 from 'uuid/v4'
import shared from './modules/shared'
import { Documents } from './modules/documents'
import { User } from './modules/user'
import { Settings } from './modules/settings'
import { AssetManagers } from './modules/assetManagers'
import { ImagesDB } from './modules/images'

Vue.use(Vuex)

let config = {
  apiKey: 'AIzaSyAr_aXeWVag0onMj7f8VyU3m4RzJ_-L5_o',
  authDomain: 'secureclientmanager.firebaseapp.com',
  databaseURL: 'https://secureclientmanager.firebaseio.com',
  projectId: 'secureclientmanager',
  storageBucket: 'secureclientmanager.appspot.com',
  messagingSenderId: '93627956906'
}

export const store = new Vuex.Store({
  state: {
    hasSettings: false,
    user: null,
    dbName: 'cim.db',
    passkey: null,
    fb: null,
    bucket: null,
    gcs: null
  },
  mutations: {
    setUser (state, payload) {
      if (payload) {
        store.dispatch('settings/setEmail', payload.email)
        store.dispatch('settings/setVerified', payload.verified)
        state.user = payload
        // let loaded = store.getters['docs/loaded']
        // console.log(`loaded ${loaded}`)
        if (payload.verified && !state.loaded) {
          // console.log('verified and not loaded')
          const hash = crypto.createHash('sha256')
          let emailpass = hash.update(state.user.email).digest('hex')
          // console.log(`Email pass: ${emailpass}`)
          state.fb.database().ref('/users/' + emailpass).once('value').then((snapshot) => {
            console.log(`from firebase: ${JSON.stringify(snapshot.val())}`)
            state.passkey = snapshot.val().dbkey
            if (!state.passkey) {
              // console.log('CREATE NEW KEY')
              state.passkey = uuidv4()
              let data = {id: state.user.uid, dbkey: state.passkey}
              state.fb.database().ref('users/' + emailpass).set(data)
            }
            store.dispatch('user/authStateChange')
          })
        }
      } else {
        state.user = null
      }
    },
    updateUser (state, payload) {
      state.user = payload
    },
    setVerified (state, payload) {
      if (state.user) {
        state.user.verified = payload
      }
      store.dispatch('settings/setVerified', payload.verified)
    },
    setEmailSent (state, payload) {
      state.user.emailSent = payload
      store.dispatch('settings/setEmailSent', payload)
    },
    load (state) {
      // open/create db
      // console.log('index.js - load documents')
      if (!store.getters['docs/loaded']) {
        // console.log('load documents ' + state.dbName + ' key ' + state.passkey)
        store.dispatch('docs/load') // , {dbName: state.dbName, key: state.passkey, email: state.user.email})
      }
    },
    setFirebase (state) {
      console.log('initialize firebase')
      state.fb = firebase.initializeApp(config)
    }
  },
  actions: {
    initState (context) {
      // console.log(context)
      context.commit('setFirebase')
      context.dispatch('settings/initSettings')
      context.state.hasSettings = true
      context.dispatch('images/load')
      context.dispatch('asset/loadAssetManagers')
    },
    signOut (context) {
      context.dispatch('user/signOut')
    },
    load ({commit}) {
      commit('load')
    }
  },
  getters: {
    getUser (state) {
      return state.user
    },
    isSignedIn (state) {
      // has uid and verified
      // console.log(`check if loaded ${state.user} && ${state.user.uid} && ${state.user.verified} && ${store.getters['docs/loaded']}`)
      return state.user && state.user.uid && state.user.verified && store.getters['docs/loaded']
    },
    isVerified (state) {
      return (state.user && state.user.verified) || (store.getters['settings/isVerified'])
    },
    getPassKey (state) {
      return state.passkey
    },
    /*
    isLoaded (state) {
      return state.docsLoaded
    }, */
    auth (state) {
      return state.fb.auth()
    },
    database (state, payload) {
      console.log('getter database')
      if (payload) {
        return state.fb.database(payload)
      } else {
        return state.fb.database()
      }
    }
  },
  modules: {
    shared: shared,
    asset: AssetManagers,
    user: User,
    settings: Settings,
    docs: Documents,
    images: ImagesDB
  }
})
// push user and passkey

/*
.then(() => {
          console.log('(2) init forms')
          context.dispatch('docs/initForms')
            .then(() => {
              let document = {
                key: 'data',
                fileName: 'file.pdf',
                filePath: '/home/riekie',
                title: 'this input Form',
                description: 'this is an input pdf form',
                company: 'JFundi'
              }
              context.getters['db/getConnection'].sync()
                .then(() => {
                  console.log('(3) insert document')
                  context.dispatch('docs/insertDocument', document)
                })
                .then(() => {
                  console.log('(4) close database')
                  context.dispatch('db/closeDatabase')
                })
            })
        })

----------------------
gettere: {
  loadMeetup(state) {
    return (meetupId) => {
      return state.loadedmeetups.find((meetup) => {
        return meetup.id === meetupId
        })
    }
}
in vue:
this.$store.getters.loadedMeetups
*/
