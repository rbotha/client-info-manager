import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Settings from '@/components/Settings/Settings'
import Advisers from '@/components/Advisers/Advisers'
import Clients from '@/components/Clients/Clients'
import Client from '@/components/Clients/Client'
import AddClient from '@/components/Clients/AddClient'
import Documents from '@/components/Docs/Documents'
import Document from '@/components/Docs/Document'
import ImportDocument from '@/components/Docs/ImportDocument'
import Profile from '@/components/User/Profile'
import Signin from '@/components/User/Signin'
import Signup from '@/components/User/Signup'
import AssetManagers from '@/components/AssetManager/AssetManagers'
import CreateAssetManager from '@/components/AssetManager/CreateAssetManager'
import AssetManager from '@/components/AssetManager/AssetManager'
import ImageManager from '@/components/Images/ImageManager'
import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/assetmanagers',
      name: 'AssetManagers',
      component: AssetManagers
    },
    {
      path: '/assetmanager/new',
      name: 'CreateAssetManager',
      component: CreateAssetManager,
      beforeEnter: AuthGuard
    },
    {
      path: '/assetmanagers/:id',
      name: 'AssetManager',
      props: true,
      component: AssetManager
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      beforeEnter: AuthGuard
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/signin',
      name: 'Signin',
      component: Signin
    },
    {
      path: '/documents',
      name: 'Documents',
      component: Documents
    },
    {
      path: '/document/new',
      name: 'ImportDocument',
      component: ImportDocument
    },
    {
      path: '/documents/:id',
      name: 'Document',
      props: true,
      component: Document
    },
    {
      path: '/client/new',
      name: 'AddClient',
      component: AddClient
    },
    {
      path: '/clients/:id',
      name: 'Client',
      component: Client
    },
    {
      path: '/clients',
      name: 'Clients',
      component: Clients
    },
    {
      path: '/advisers',
      name: 'Advisers',
      component: Advisers
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    },
    {
      path: '/imagemanager',
      name: 'ImageManager',
      component: ImageManager
    },
    {
      path: '/upload',
      name: 'UploadFile',
      component: ImportDocument
    },
    {
      path: '*',
      redirect: '/'
    }
  ],
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash
      }
    }
    if (savedPosition) {
      return savedPosition
    }

    return {x: 0, y: 0}
  }
})
