'use strict'

import { app, BrowserWindow, ipcMain, Menu } from 'electron'
const nativeImage = require('electron').nativeImage

const PDFWindow = require('electron-pdf-window')
const settings = require('electron-settings')
// const nativeImage = require('electron').nativeImage
const hummus = require('hummus')
const PDFDigitalForm = require('./pdf-digital-form')
// const pdfFiller = require('pdffiller-keepfields')
// const fs = require('fs')
// const crypto = require('crypto')
const path = require('path')
const email = require('emailjs')

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

let mainWindow
let addImageWindow

const menuTemplate = [
  {}, // mac
  {
    label: 'Images',
    submenu: [
      {
        label: 'Add Image',
        click () {
          createAddImageWindow()
        }
      },
      {
        label: 'Quit',
        accelerator: process.platform === 'darwin' ? 'Command+Q' : 'Ctrl+Q',
        click () {
          app.quit()
        }
      }
    ]
  }
]

if (process.platform === 'darwin') {
  menuTemplate.unshift({})
}

let server = email.server.connect({
  user: 'riekie@jfundi.com',
  password: '1L?Q+OxzBLJ2',
  host: 'sm4b.unisonplatform.com',
  ssl: true
})

console.log(winURL)

function createWindow () {
  /* let file = `./${__dirname}/placeholder.png`
  console.log(`file: ${file}`)
  const hash = crypto.createHash('md5')
  let image = nativeImage.createFromPath(file)
  let data = image.toDataURL()
  let hashid = hash.update(data).digest('hex')
  let thumb = image.resize({height: 150, quality: 'best'})
  let newImage = {
    hashid: hashid,
    filename: file,
    data: data,
    thumb: thumb.toDataURL()
  }
  fs.writeFile('defaultImage.js', JSON.stringify(newImage), (err) => {
    // throws an error, you could also catch it here
    if (err) console.log('error ' + err)

    // success case, the file was saved
    console.log('image saved!')
  })
 */

  let window = {x: 0, y: 0, height: 563, width: 1000, maximised: false, useContentSize: true, show: false}
  if (settings.has('window')) {
    window = settings.get('window')
  } else {
    settings.set('window', window)
  }
  let dir = settings.get('window.width')
  window.title = 'Client Information Manager'
  window.webPreferences = {backgroundThrottling: false}
  console.log(`APP DIR===================${dir}`)
  // let appPath = app.getPath('userData')
  // app.setPath('userData', `${appPath}/cim`)
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow(window)

  mainWindow.once('ready-to-show', () => {
    if (window.maximized) {
      mainWindow.maximize()
    }
    mainWindow.show()
  })
  mainWindow.loadURL(winURL)

  const mainMenu = Menu.buildFromTemplate(menuTemplate)
  Menu.setApplicationMenu(mainMenu)
  mainWindow.on('close', () => {
    console.log('received close event')
    mainWindow.webContents.send('signout')
    let bounds = mainWindow.getBounds()
    settings.set('window', {
      x: bounds.x,
      y: bounds.y,
      height: bounds.height,
      width: bounds.width,
      maximized: mainWindow.isMaximized(),
      useContentSize: true
    })
  })
  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

function createAddImageWindow () {
  addImageWindow = new BrowserWindow({
    width: 450,
    height: 600,
    title: 'Add new image'
  })
  addImageWindow.loadURL(`file://${__dirname}/add.html`)
}

ipcMain.on('close', () => {
  app.quit()
})

ipcMain.on('viewPDF', (event, file) => {
  const win = new PDFWindow({
    width: 800,
    height: 600,
    webPreferences: {
      devTools: false
    }
  })
  console.log(`pdf file: ${file}`)
  PDFWindow.addSupport(win)
  try {
    win.loadURL(file)
  } catch (err) {
    console.log(`load rejected: ${err}`)
  }
})

ipcMain.on('parse-pdf', (event, file) => {
  if (path.extname(file).toLowerCase() === '.pdf') {
    // let noExt = path.basename(file, path.extname(file))
    let pdfParser = hummus.createReader(file)
    let digitalForm = new PDFDigitalForm(pdfParser)
    if (digitalForm.hasForm()) {
      // console.log(digitalForm.createSimpleKeyValue())
      // console.log(JSON.stringify(digitalForm.fields, null, 2))
      /*
      {
      "name": "RABenefit3",
      "fullName": "RABenefit3",
      "isNoExport": false,
      "isFileSelect": false,
      "type": "plaintext", //radio, checkbox, push, choice, signature, richtext
      "value": null
      }
      */
      let fields = []
      digitalForm.fields.forEach((field) => {
        console.log(`Name: ${field.name} type: ${field.type}`)
        fields.push({name: field.name, fullName: field.fullName, type: field.type})
      })
      mainWindow.webContents.send('pdf-info', fields)
      /*
       {"Title":null,"Surname":null,"Initials":null
       ,"Nickname":null,"Occupation":null,"Name":null,"IdPassportNumber":null, ...
      */
    }
  }
})
ipcMain.on('image', (event, file) => {
  let image = nativeImage.createFromPath(file)
  // console.log(image.getSize())
  // let buf = image.toBitmap()
  let original = image.toDataURL()
  image = image.resize({width: 150, height: 150, quality: 'best'})
  mainWindow.webContents.send('image-data', {image: original, thumb: image.toDataURL()})
  // console.log(image.toDataURL())
})

ipcMain.on('send-email', (event, email) => {
  server.send({
    text: email.message,
    from: 'riekie <riekie@jfundi.com>',
    to: email.address,
    subject: email.subject
  }, (err, message) => {
    console.log(err || message)
  })
})
/*
app.on('login', (event, webContents, request, authInfo, callback) => {
  event.preventDefault()
  callback('username', 'secret')
})
     clipboard
     crashReporter
     desktopCapturer
     ipcRenderer
     nativeImage
     remote
     screen
     shell
     webFrame

    To use Gmail's SMTP server, you will need the following settings for your outgoing emails:

    Outgoing Mail (SMTP) Server: smtp.gmail.com
    Use Authentication: Yes
    Use Secure Connection: Yes (TLS or SSL depending on your mail client/website SMTP plugin)
    Username: your Gmail account (e.g. user@gmail.com)
    Password: your Gmail password
    Port: 465 (SSL required) or 587 (TLS required)

*/
/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
